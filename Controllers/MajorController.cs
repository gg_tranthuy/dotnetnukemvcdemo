﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Christoc.Modules.SampleTesting.Components;
using Christoc.Modules.SampleTesting.Models;
using DotNetNuke.Web.Mvc.Framework.ActionFilters;
using DotNetNuke.Web.Mvc.Framework.Controllers;

namespace Christoc.Modules.SampleTesting.Controllers
{
    public class MajorController : DnnController
    {
        [ModuleAction(ControlKey = "AddMajor", TitleKey = "AddNewMajor")]
        public ActionResult Index()
        {
            var major = MajorManager.Instance.GetMajors(ModuleContext.ModuleId);
            return View(major);
        }

        [HttpGet]
        public ActionResult AddMajor()
        {
            var major = new Major {ModuleId = ModuleContext.ModuleId};
            return View(major);
        }

        [HttpPost]
        public ActionResult AddMajor(Major m)
        {
            MajorManager.Instance.CreateMajor(m);
            return RedirectToDefaultRoute();
        }

        [HttpGet]
        public ActionResult Edit(int majorId)
        {
            var major  = MajorManager.Instance.GetMajor(majorId, ModuleContext.ModuleId);
            return View(major);
        }

        [HttpPost]
        public ActionResult Edit(Major m)
        {
            var majorUpdate = MajorManager.Instance.GetMajor(m.MajorId, m.ModuleId);
            majorUpdate.MajorName = m.MajorName;
            MajorManager.Instance.UpdateMajor(majorUpdate);
            return RedirectToDefaultRoute();
        }
    }
}