﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Christoc.Modules.SampleTesting.Components;
using Christoc.Modules.SampleTesting.Models;
using DotNetNuke.Web.Mvc.Framework.ActionFilters;
using DotNetNuke.Web.Mvc.Framework.Controllers;

namespace Christoc.Modules.SampleTesting.Controllers
{
    public class StudentController : DnnController
    {
        [ModuleAction(ControlKey = "AddStudent", TitleKey = "AddNewStudent")]
        public ActionResult Index(int id)
        {
            ViewBag.ModuleId = id;
            var student = StudentManager.Instance.GetStudents(ModuleContext.ModuleId).Where(s=>s.MajorId==id);
            return View(student);
        }

        [HttpGet]
        public ActionResult AddStudent(int id)
        {
            var student = new Student { ModuleId = ModuleContext.ModuleId, MajorId = id};
            return View(student);
        }

        [HttpPost]
        public ActionResult AddStudent(Student s)
        {
            StudentManager.Instance.CreateStudent(s);
            return RedirectToDefaultRoute();
        }
    }
}