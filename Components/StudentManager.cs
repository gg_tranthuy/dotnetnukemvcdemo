﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Christoc.Modules.SampleTesting.Models;
using DotNetNuke.Data;
using DotNetNuke.Framework;

namespace Christoc.Modules.SampleTesting.Components
{
    interface IStudent
    {
        void CreateStudent(Student s);
        void DeleteStudent(int studentId, int moduleId);
        void DeleteStudent(Student s);
        IEnumerable<Student> GetStudents(int moduleId);
        Student GetStudent(int studentId, int moduleId);
        void UpdateStudent(Student s);
    }
    class StudentManager : ServiceLocator<IStudent, StudentManager>, IStudent

    {
        public void CreateStudent(Student s)
        {
            using (var context = DataContext.Instance())
            {
                var repo = context.GetRepository<Student>();
                repo.Insert(s);
            }
        }

        public void DeleteStudent(int studentId, int moduleId)
        {
            var student = GetStudent(studentId, moduleId);
            DeleteStudent(student);
        }

        public void DeleteStudent(Student s)
        {
            using (var context = DataContext.Instance())
            {
                var repo = context.GetRepository<Student>();
                repo.Delete(s);
            }
        }

        public Student GetStudent(int studentId, int moduleId)
        {
            Student student;
            using (var context = DataContext.Instance())
            {
                var repo = context.GetRepository<Student>();
                student = repo.GetById(studentId, moduleId);
            }
            return student;
        }

        public IEnumerable<Student> GetStudents(int moduleId)
        {
            IEnumerable<Student> lstStudent;
            using (var context = DataContext.Instance())
            {
                var repo = context.GetRepository<Student>();
                lstStudent = repo.Get(moduleId);
            }
            return lstStudent;
        }

        public void UpdateStudent(Student s)
        {
            using (var context = DataContext.Instance())
            {
                var repo = context.GetRepository<Student>();
                repo.Update(s);
            }
        }

        protected override Func<IStudent> GetFactory()
        {
            return () => new StudentManager();
        }
    }
}