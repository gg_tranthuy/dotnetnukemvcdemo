﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Christoc.Modules.SampleTesting.Models;
using DotNetNuke.Data;
using DotNetNuke.Framework;

namespace Christoc.Modules.SampleTesting.Components
{
    interface IMajor
    {
        void CreateMajor(Major m);
        void DeleteMajor(int majorId, int moduleId);
        void DeleteMajor(Major m);
        IEnumerable<Major> GetMajors(int moduleId);
        Major GetMajor(int majorId, int moduleId);
        void UpdateMajor(Major m);
    }
    class MajorManager : ServiceLocator<IMajor, MajorManager>, IMajor

    {
        public void CreateMajor(Major m)
        {
            using (var context = DataContext.Instance())
            {
                var repo = context.GetRepository<Major>();
                repo.Insert(m);
            }
        }

        public void DeleteMajor(int majorId, int moduleId)
        {
            var major = GetMajor(majorId, moduleId);
            DeleteMajor(major);
        }

        public void DeleteMajor(Major m)
        {
            using (var context = DataContext.Instance())
            {
                var repo = context.GetRepository<Major>();
                repo.Delete(m);
            }
        }

        public Major GetMajor(int majorId, int moduleId)
        {
            Major major;
            using (var context = DataContext.Instance())
            {
                var repo = context.GetRepository<Major>();
                major = repo.GetById(majorId, moduleId);
            }
            return major;
        }

        public IEnumerable<Major> GetMajors(int moduleId)
        {
            IEnumerable<Major> lstMajor;
            using (var context = DataContext.Instance())
            {
                var repo = context.GetRepository<Major>();
                lstMajor = repo.Get(moduleId);
            }
            return lstMajor;
        }

        public void UpdateMajor(Major m)
        {
            using (var context = DataContext.Instance())
            {
                var repo = context.GetRepository<Major>();
                repo.Update(m);
            }
        }

        protected override Func<IMajor> GetFactory()
        {
            return ()=> new MajorManager();
        }
    }
}