﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using DotNetNuke.ComponentModel.DataAnnotations;

namespace Christoc.Modules.SampleTesting.Models
{
    [TableName("Major")]
    //setup the primary key for table
    [PrimaryKey("MajorId", AutoIncrement = true)]
    //configure caching using PetaPoco
    [Cacheable("Majors", CacheItemPriority.Default, 20)]
    //scope the objects to the ModuleId of a module on a page (or copy of a module on a page)
    [Scope("ModuleId")]
    public class Major
    {
        public int MajorId { get; set; }
        public string MajorName { get; set; }
        public int ModuleId { get; set; }
    }
}