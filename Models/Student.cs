﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Caching;
using DotNetNuke.ComponentModel.DataAnnotations;

namespace Christoc.Modules.SampleTesting.Models
{
    [TableName("Student")]
    //setup the primary key for table
    [PrimaryKey("StudentId", AutoIncrement = true)]
    //configure caching using PetaPoco
    [Cacheable("Students", CacheItemPriority.Default, 20)]
    //scope the objects to the ModuleId of a module on a page (or copy of a module on a page)
    [Scope("ModuleId")]
    public class Student
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public int StudentAge { get; set; }
        public string StudentAddress { get; set; }
        public int ModuleId { get; set; }
        public int MajorId { get; set; }
    }
}