﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DotNetNuke.Web.Mvc.Routing;

namespace Christoc.Modules.SampleTesting
{
    public class RouteConfig : IMvcRouteMapper
    {
        public void RegisterRoutes(IMapRoute mapRouteManager)
        {
            mapRouteManager.MapRoute("SampleTesting", "Routing", "{controller}/{action}",
                new[] {"Christoc.Modules.SampleTesting.Controllers"});
        }
    }
}